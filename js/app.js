var dashboardApp = angular.module('dashboardApp', ['ngRoute', 'ngAnimate']);

dashboardApp.controller('dashboardCtrl', function($scope){
    $scope.actions = [
        {
            action: 'new article',
            description: 'Article has been added',
            thumbnail: 'img/article.png',
            date: new Date()
        },
        {
            action: 'new comment',
            description: 'Comment has been added',
            thumbnail: 'img/comment.png',
            date: new Date()
        },
        {
            action: 'new file',
            description: 'File has been added',
            thumbnail: 'img/file.png',
            date: new Date()
        }
    ]

    $scope.comments = [
        {
            action: 'Comment added by John Doe',
            description: 'This is sample comment',
            thumbnail: 'img/comment.png',
            date: new Date()
        },
        {
            action: 'Comment added by John Doe',
            description: 'This is sample comment',
            thumbnail: 'img/comment.png',
            date: new Date()
        }
    ]

    $scope.articles = [
        {
            action: 'Article added by John Doe',
            description: 'This is article title',
            thumbnail: 'img/article.png',
            date: new Date()
        },
        {
            action: 'Article added by John Doe',
            description: 'This is article title',
            thumbnail: 'img/article.png',
            date: new Date()
        },
        {
            action: 'Article added by John Doe',
            description: 'This is article title',
            thumbnail: 'img/article.png',
            date: new Date()
        }
    ]

    $scope.files = [
        {
            action: 'File added by John Doe',
            description: 'file name',
            thumbnail: 'img/file.png',
            date: new Date()
        },
        {
            action: 'File added by John Doe',
            description: 'file name',
            thumbnail: 'img/file.png',
            date: new Date()
        }
    ]
}).controller('usageCtrl', function($scope){
    var data = {
    labels: [
        "Folders",
        "Files",
        "Articles",
        "Comments"
    ],
    datasets: [
        {
            data: [23, 123, 67, 232],
            backgroundColor: [
                "lightgray",
                "black",
                "#777",
                "#eee"
            ],
            hoverBackgroundColor: [
                "lightgray",
                "black",
                "#777",
                "#eee"
            ]
        }]
    };

    var options = {
        legend: {
                display: true,
                labels: {
                    fontSize: 20
                }
            }  
    };

    var ctx = document.getElementById("usageDoughnut");
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        options: options
    });
}).controller('usersCtrl', function($scope, $timeout){
    $scope.userList = [
        {
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com'
        },
        {
            firstName: 'Jan',
            lastName: 'Kowalski',
            email: 'jan.kowalski@example.com'
        },
        {
            firstName: 'Adam',
            lastName: 'Smith',
            email: 'adam.smith@example.com'
        },
        {
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com'
        },
        {
            firstName: 'Jan',
            lastName: 'Kowalski',
            email: 'jan.kowalski@example.com'
        },
        {
            firstName: 'Adam',
            lastName: 'Smith',
            email: 'adam.smith@example.com'
        },
        {
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com'
        },
        {
            firstName: 'Jan',
            lastName: 'Kowalski',
            email: 'jan.kowalski@example.com'
        },
        {
            firstName: 'Adam',
            lastName: 'Smith',
            email: 'adam.smith@example.com'
        },
        {
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com'
        },
        {
            firstName: 'Jan',
            lastName: 'Kowalski',
            email: 'jan.kowalski@example.com'
        },
        {
            firstName: 'Adam',
            lastName: 'Smith',
            email: 'adam.smith@example.com'
        },
        {
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com'
        },
        {
            firstName: 'Jan',
            lastName: 'Kowalski',
            email: 'jan.kowalski@example.com'
        },
        {
            firstName: 'Adam',
            lastName: 'Smith',
            email: 'adam.smith@example.com'
        },
        {
            firstName: 'John',
            lastName: 'Doe',
            email: 'john.doe@example.com'
        },
        {
            firstName: 'Jan',
            lastName: 'Kowalski',
            email: 'jan.kowalski@example.com'
        },
        {
            firstName: 'Adam',
            lastName: 'Smith',
            email: 'adam.smith@example.com'
        },
    ];

    $scope.removeUser = function(user){
       var idx =  $scope.userList.indexOf(user);
       $scope.userList.splice(idx, 1);
       $scope.userRemoved = true;
       $timeout(function(){$scope.userRemoved = false}, 2000);
    }

}).controller('settingsCtrl', function($scope){

}).controller('activityCtrl', function($scope){
    $scope.actions = [
        {
            action: 'new article',
            description: 'Article has been added',
            thumbnail: 'img/article.png',
            date: new Date()
        },
        {
            action: 'new comment',
            description: 'Comment has been added',
            thumbnail: 'img/comment.png',
            date: new Date()
        },
        {
            action: 'new file',
            description: 'File has been added',
            thumbnail: 'img/file.png',
            date: new Date()
        },
        {
            action: 'new comment',
            description: 'Comment has been added',
            thumbnail: 'img/comment.png',
            date: new Date()
        },
        {
            action: 'new comment',
            description: 'Comment has been added',
            thumbnail: 'img/comment.png',
            date: new Date()
        },
        {
            action: 'new comment',
            description: 'Comment has been added',
            thumbnail: 'img/comment.png',
            date: new Date()
        }
    ]
}).controller('statisticsCtrl', function($scope){
    var dataUsers = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "Users",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "lightgray",
                borderColor: "lightgray",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "lightgray",
                pointBackgroundColor: "lightgray",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "lightgray",
                pointHoverBorderColor: "black",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [10, 24, 37, 29, 35, 45, 46],
                spanGaps: false,
            }
        ]
    };

    var dataContent = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "Content",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "lightgray",
                borderColor: "lightgray",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "lightgray",
                pointBackgroundColor: "lightgray",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "lightgray",
                pointHoverBorderColor: "black",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: [128, 467, 1123, 4023, 5677, 6789, 7400],
                spanGaps: false,
            }
        ]
    };

    var optionsGeneral = {
            legend: {
                display: true,
                labels: {
                    fontSize: 20
                }
            }  
        }

    var ctxUsers = document.getElementById("usersChart");
    var usersChart = new Chart(ctxUsers, {
        type: 'line',
        data: dataUsers,
        options: optionsGeneral
    });

    var ctxContent = document.getElementById("contentChart");
    var contentChart = new Chart(ctxContent, {
        type: 'line',
        data: dataContent,
        options: optionsGeneral
    });
}).controller('contentCtrl', function($scope){
    $scope.folders = {
        count: 120,
        list: [
            {name: 'folder1'},
            {name: 'folder2'},
            {name: 'folder3'},
        ]
    };

    $scope.files = {
        count: 389,
        list: [
            {name: 'file1'},
            {name: 'file2'},
            {name: 'file3'},
        ]
    };

    $scope.articles = {
        count: 34,
        list: [
            {name: 'article1'},
            {name: 'article2'},
            {name: 'article3'},
        ]
    };

    $scope.comments = {
        count: 1120,
        list: [
            {name: 'comment1'},
            {name: 'comment2'},
            {name: 'comment3'},
        ]
    };

}).config(['$routeProvider','$locationProvider', function($routeProvider, $locationProvider){
    //$locationProvider.html5Mode(true);
    $routeProvider.when('/', {controller: 'dashboardCtrl', templateUrl: '../dashboard.html'})
    .when('/dashboard', {controller: 'dashboardCtrl', templateUrl: '../dashboard.html'})
    .when('/content', {controller: 'contentCtrl', templateUrl: '../content.html'})
    .when('/activity', {controller: 'activityCtrl', templateUrl: '../activity.html'})
    .when('/statistics', {controller: 'statisticsCtrl', templateUrl: '../statistics.html'})
    .when('/settings', {controller: 'settingsCtrl', templateUrl: '../settings.html'})
    .when('/usage', {controller: 'usageCtrl', templateUrl: '../usage.html'})
    .when('/users', {controller: 'usersCtrl', templateUrl: '../users.html'}).
    otherwise({redirectTo: '../dashboard.html'});
}]);

